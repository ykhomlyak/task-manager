# README #

* npm should be installed
* run 'npm install' in project directory to install dependencies
* run 'grunt build' to launch development task (just watch all files for changes and run livereload)
* run 'grunt build --env=production' to build production directory (compile requirejs, minify css etc.)