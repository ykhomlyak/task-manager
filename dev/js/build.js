require.config({
  deps: ['app'],
  urlArgs: "t=" + (new Date()).getTime(),
  paths: {
    jquery: 'lib/jquery-1.11.0.min',
    underscore: 'lib/underscore-min',
    backbone: 'lib/backbone-min',
    localStorage: 'lib/backbone.localStorage-min',
    text: 'lib/require.text'
  },
  shim: {
    underscore: {
      exports: '_'
    },
    backbone: {
      deps: ["underscore", "jquery"],
      exports: "Backbone"
    }
  }
});