require([
  'jquery',
  'underscore',
  'backbone',
  'routers/router'
], function($, _, Backbone, AppRouter) {
  $(function() {
    new AppRouter();
    Backbone.history.start();
  });
});