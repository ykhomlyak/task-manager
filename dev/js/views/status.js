define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/status.html'
], function($, _, Backbone, itemTpl) {
  return Backbone.View.extend({
    tagName: 'li',
    className: 'list-group-item',
    attributes: function() {
      return {draggable: this.model.get('status') !== 3};
    },
    template: _.template(itemTpl),
    events: {
      'click .remove' : 'destroy',
      'dragstart': 'dragstart'
    },
    initialize: function() {
      this.listenTo(this.model, 'destroy', this.remove);
    },
    render: function() {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    },
    destroy: function() {
      this.model.destroy();
    },
    dragstart: function(e) {
      var data = {
        modelId: this.model.get('id'),
        parentId: e.target.parentNode.id,
        targetIndex: $(e.target).index()
      };

      e.originalEvent.dataTransfer.effectAllowed = 'move';
      e.originalEvent.dataTransfer.setData('Text', JSON.stringify(data));
    }
  });
});