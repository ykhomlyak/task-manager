define([
  'jquery',
  'underscore',
  'backbone',
  'collections/tasks',
  'views/statusList',
  'text!templates/main.html'
], function($, _, Backbone, Tasks, StatusListView, mainTpl) {
  return Backbone.View.extend({
    template: _.template(mainTpl),
    events: {
      'submit #add-new': 'addNew',
      'drop .list-group': 'drop',
      'dragover .list-group': 'dragOver',
      'dragleave .list-group': 'dragLeave'
    },
    initialize: function() {
      this.listenTo(Tasks, 'add', this.addToPending);
    },
    render: function() {
      this.$el.html(this.template());
      $('#holder').html(this.$el);

      this.taskName = this.$('#new-task');

      this.pendingList = new StatusListView({
        el: '#pending',
        attributes: {dataStatus: 1}
      });

      new StatusListView({
        el: '#in-progress',
        attributes: {dataStatus: 2}
      });

      new StatusListView({
        el: '#finished',
        attributes: {dataStatus: 3}
      });

      Tasks.fetch({reset: true});
    },
    addNew: function(e) {
      e.preventDefault();

      var taskName = this.taskName;

      if(taskName.val().trim()) {
        Tasks.create({
          title: taskName.val().trim(), id: Tasks.nextId()
        });
        taskName.val('').focus();
      }
    },
    addToPending: function(task) {
      this.pendingList.addOne(task);
    }
  });
});