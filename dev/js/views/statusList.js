define([
  'jquery',
  'underscore',
  'backbone',
  'collections/tasks',
  'views/status'
], function($, _, Backbone, Tasks, StatusView) {
  return Backbone.View.extend({
    events: {
      'drop': 'drop',
      'dragover': 'dragover',
      'dragleave': 'dragleave'
    },
    initialize: function() {
      this.listenToOnce(Tasks, 'reset', this.addAll);
      this.$el.data('status', this.attributes.dataStatus);
    },
    addOne: function(task) {
      var view = new StatusView({model: task});

      this.$el.append(view.render().el);
    },
    addAll: function() {
      this.$el.empty();
      _.each(Tasks.getStatusList(this.attributes.dataStatus), this.addOne, this);
    },
    drop: function(e) {
      e.preventDefault();

      var status = this.$el.removeClass('dragover').data('status'),
        data = JSON.parse(e.originalEvent.dataTransfer.getData('Text'));

      // prevent dropping in the same status
      if(data.parentId !== e.currentTarget.id) {
        this.listenToOnce(Tasks, 'change', this.addOne);
        Tasks.get(data.modelId).changeStatus(status);
        $('#' + data.parentId).children().eq(data.targetIndex).remove();
      }
    },
    dragover: function(e) {
      e.preventDefault();

      $(e.currentTarget).addClass('dragover');
    },
    dragleave: function(e) {
      e.preventDefault();

      $(e.currentTarget).removeClass('dragover');
    }
  });
});