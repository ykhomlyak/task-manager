define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/notfound.html'
], function($, _, Backbone, notFoundTpl) {
  return Backbone.View.extend({
    template: _.template(notFoundTpl),
    render: function() {
      this.$el.html(this.template());
      $('#holder').html(this.$el);
    }
  });
});