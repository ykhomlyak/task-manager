define([
  'underscore',
  'backbone',
  'localStorage',
  'models/task'
], function(_, Backbone, Store, Task) {
  return new (Backbone.Collection.extend({
    model: Task,
    localStorage: new Backbone.LocalStorage('task-manager'),
    nextId: function() {
      // get max id from records for new item
      return this.length ? (Math.max.apply(Math, this.localStorage.records) + 1) : 1;
    },
    getStatusList: function(status) {
      return this.where({status: status});
    }
  }));
});