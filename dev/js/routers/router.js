define([
  'underscore',
  'backbone',
  'views/main',
  'views/404'
], function(_, Backbone, MainView, NotFoundView) {
  return Backbone.Router.extend({
    routes: {
      '': 'main',
      '*other': 'notFound'
    },
    main: function() {
      new MainView().render();
    },
    notFound: function() {
      new NotFoundView().render();
    }
  });
});