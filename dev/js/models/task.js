define(['underscore', 'backbone'], function(_, Backbone) {
  return Backbone.Model.extend({
    defaults: {
      title: 'new task',
      status: 1
    },
    changeStatus: function(status) {
      this.save({status: status});
    }
  });
});