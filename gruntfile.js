module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      css: {
        files: 'dev/**/*.css',
        options: {
          livereload: true
        }
      },
      scripts: {
        files: ['dev/**/*.js'],
        options: {
          livereload: true
        }
      },
      html: {
        files: ['dev/**/*.html'],
        options: {
          livereload: true
        }
      }
    },
    processhtml: {
      dist: {
        files: {
          'production/index.html': ['dev/index.html']
        }
      }
    },
    cssmin: {
      merge: {
        files: {
          'production/assets/css/app.min.css': ['dev/assets/css/bootstrap.min.css', 'dev/assets/css/app.css']
        }
      }
    },
    copy: {
      assets: {
        files: [
          {
            expand: true,
            cwd: 'dev/assets',
            src: ['**', '!css/**'],
            dest: 'production/assets'
          }
        ]
      },
      lib: {
        files: [
          {
            expand: true,
            cwd: 'dev/js/lib',
            src: ['require.js'],
            dest: 'production/js/lib'
          }
        ]
      }
    },
    requirejs: {
      compile: {
        options: {
          mainConfigFile: 'dev/js/build.js',
          baseUrl: 'dev/js',
          include: ['build'],
          out: 'production/js/main.min.js'
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-processhtml');
  grunt.loadNpmTasks('grunt-contrib-copy');

  grunt.registerTask('development', ['watch']);
  grunt.registerTask('production', ['requirejs', 'copy', 'cssmin', 'processhtml']);

  grunt.registerTask('build', function() {
    var target = grunt.option('env') || 'development';

    grunt.task.run(target);
  });
};